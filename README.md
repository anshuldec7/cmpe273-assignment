# cmpe273-assignment3


#Get /v1/expenses/{expense_id} Output
##Instance1 output

 - Request 1
```
   <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
   <title>404 Not Found</title>
   <h1>Not Found</h1>
   <p>The requested URL was not found on the server.  If you entered the URL manually please check      your spelling and try again.</p>
```
 - Request 2
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 2, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 3
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 3, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 4
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 5
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 5, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 6
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 6, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 7
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 8
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 8, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 9
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 10
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 10, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
##Instance2 output

 - Request 1
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 1, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 2
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 3
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 4
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 4, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 5
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 6
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 7
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 7, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 8
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 9
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 10
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
##Instance3 Output

 - Request 1
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 2
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 3
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 4
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 5
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 6
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 7
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 8
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```
 - Request 9
```
{
  "category": "office supplies", 
  "decision_date": "", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 9, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "Pending", 
  "submit_date": "12-10-2016"
}
```
 - Request 10
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```

#Steps for execution
 - Create Different Application servers (I have created three separate directories to create Separate Docker Images)
    -  cd /Users/\<username>/cmpe273-assignment3/server5001/
    -  docker build -t server1:latest .
    -  cd /Users/\<username>/cmpe273-assignment3/server5002/
    -  docker build -t server2:latest .
    -  cd /Users/\<username>/cmpe273-assignment3/server5003/
    -  docker build -t server3:latest .
 - Make separate path/directories for MySQL docker images
    -  mkdir /Users/\<username>/mysqlDocker1
    -  mkdir /Users/\<username>/mysqlDocker2
    -  mkdir /Users/\<username>/mysqlDocker3
 - Create/Run MySQL docker images fetched from the Docker Hub
    -  docker run --detach -p 3301:3306 --name=db1 --env="MYSQL_ROOT_PASSWORD=\<myapssword>" -v /Users/\<username>/mysqlDocker1:/var/lib/mysql mysql
    -  docker run --detach -p 3302:3306 --name=db2 --env="MYSQL_ROOT_PASSWORD=\<myapssword>" -v /Users/\<username>/mysqlDocker2:/var/lib/mysql mysql
    -  docker run --detach -p 3303:3306 --name=db3 --env="MYSQL_ROOT_PASSWORD=\<myapssword>" -v /Users/\<username>/mysqlDocker3:/var/lib/mysql mysql
 - Excecuting & Linking Application to the MySQL DB instances
    -  docker run -detach --name servertest1 --link db1:mysql -p 5001:5001  server1
    -  docker run -detach --name servertest2 --link db2:mysql -p 5002:5002  server2
    -  docker run -detach --name servertest3 --link db3:mysql -p 5003:5003  server3
 - Now the app servers are up and running execute httpclient.py to test the desired output.